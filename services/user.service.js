var { saveUserData,getUserData,updateUserData } = require("../repositories/user.repository");
var getData = function(name){
  if(name!=undefined){
    var data=getUserData();
    if (data) return data.filter(function(elem){
        return name==elem.name;
    })[0];
  }
else return null;
};
var getAll=function(){
  return getUserData();
}
var saveData = function(user){
  if (user) {
    return saveUserData(user);
  } else {
    return null;
  }
};
var updateData=function(name){
  if(getData(name)){
    return updateUserData(name);
  }
}
module.exports = {
  getData,
  saveData,
  getAll,
  updateData
};