var express = require('express');
var router = express.Router();
var { getData } = require("../services/fighter.service");
router.get('/', function(req, res, next) {
    var name=req.query.name;
    var fighter=getData(name);
    if(fighter) res.render('../fighter.pug',{fighter});
  });
module.exports=router;