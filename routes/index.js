var express = require('express');
var path=require("path");
var router = express.Router();

/* GET home page. */

router.get('/', function(req, res, next) {
  if(req.session.name==undefined) res.redirect('/login');
  else res.sendFile(path.resolve(__dirname+'./../public/index.html'));
});
router.put('/', function(req, res, next) {
  if(req.session.name!=undefined) res.redirect(307,'/user/'+req.session.name);
});
//как узнать req.session.name делая запрос в fightView?
module.exports = router;
